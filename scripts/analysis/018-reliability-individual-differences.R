# This script estimates the reliability of individual participant profiles 
# on the basis of the baseline harmonic tones experiment.

library(tidyverse)
library(ggpubr)

theme_set(theme_pubr())

source("src/DyadRating.R")
Rcpp::sourceCpp("src/smooth_2d_gaussian.cpp")


df <- 
  read_csv("input/data-csv/Rating/rating_dyh3dd.csv", col_types = cols()) |> 
  rename(interval = v1)


int_range <- c(0, 15)
resolution <- 1000
smooth_bandwidth <- 0.5


fit_participant_profile <- function(
  participant_data,
  int_range,
  resolution,
  smooth_bandwidth
) {
  tibble(
    interval = seq(
      from = int_range[1],
      to = int_range[2],
      length.out = resolution
    ),
    rating = smooth_2d_gaussian(
      data_x = participant_data$interval,
      data_y = rep(0, times = nrow(participant_data)),
      data_val = participant_data$rating,
      probe_x = interval, 
      probe_y = rep(0, times = length(interval)),
      sigma_x = smooth_bandwidth,
      sigma_y = smooth_bandwidth
    )
  )
}

get_participant_profile_split_half_reliability <- function(
    participant_data,
    int_range,
    resolution,
    smooth_bandwidth
) {
  n_ratings <- nrow(participant_data)
  if (n_ratings < 2) {
    return(NA_real_)
  } 
  groups <- rep_len(1:2, length.out = n_ratings)
  groups <- groups[sample(length(groups), size = length(groups), replace = FALSE)]
  grouped_data <- split(participant_data, groups)
  profiles <- 
    map(grouped_data, fit_participant_profile, int_range, resolution, smooth_bandwidth) |> 
    map("rating")
  suppressWarnings(cor(profiles[[1]], profiles[[2]]))
}

get_participant_profile_split_half_reliability_monte_carlo <- function(
    participant_data,
    int_range,
    resolution,
    smooth_bandwidth,
    n = 100
) {
  map_dbl(
    seq_len(n), 
    ~ get_participant_profile_split_half_reliability(
      participant_data,
      int_range,
      resolution,
      smooth_bandwidth
    ),
    .progress = TRUE
  ) |> 
    mean(na.rm = TRUE)
}

data_by_participant <- split(df, df$participant_id) |> set_names(NULL)

set.seed(1234)

reliability_by_participant <- plyr::laply(
  data_by_participant,
  get_participant_profile_split_half_reliability_monte_carlo,
  int_range,
  resolution,
  smooth_bandwidth,
  .progress = "time"
)

print(median(reliability_by_participant, na.rm = TRUE))
