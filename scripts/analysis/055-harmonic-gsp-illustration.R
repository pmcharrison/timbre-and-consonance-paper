source("scripts/analysis/015-figure-setup.R")
source("src/DyadRating.R")
source("src/utils.R")
source("src/plots.R")
Rcpp::sourceCpp("src/smooth_2d_gaussian.cpp")
library(magick)

HARMONIC_DYADS <- EXPERIMENTS$`Harmonic dyads (3 dB roll-off)`
HARMONIC_TRIADS <- EXPERIMENTS$`Harmonic triads (3 dB roll-off)`


p <- HARMONIC_TRIADS$behaviour$summary %>%
  ggplot(aes(
    interval_1, 
    interval_2, 
    fill = relative_density
  )) + 
  geom_raster() + 
  geom_point(aes(interval_1,interval_2),
             data = HARMONIC_TRIADS$behaviour$full$data %>% filter(degree > 1),
             size = 0.005, colour = "white", alpha = 0.15, inherit.aes = FALSE) + # 0.2
  scale_fill_viridis_c("Pleasantness") +
  scale_x_continuous(breaks=0:9) +
  scale_y_continuous(breaks=0:9) +
  grids(linetype = "dashed", size = 0.5) + # 1
  labs(x="Lower interval (semitones)",
       y="Upper interval (semitones)") +
  theme(aspect.ratio = 1)

ggsave("055-harmonic-gsp-illustration.pdf", path = PLOT_DIR, plot = p, width = 7, height = 7, dpi = 1000)
