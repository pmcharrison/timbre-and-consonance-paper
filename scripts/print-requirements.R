library(tidyverse)

jsonlite::fromJSON("renv.lock")$Packages |> 
  map_chr(~ paste(.$Package, .$Version, sep = "=")) |>
  paste(collapse = ", ")
